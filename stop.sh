#!/bin/bash
# *********************************************************
# * Author        : LEI Sen
# * Email         : sen.lei@outlook.com
# * Create time   : 2022-09-22 17:08
# * Last modified : 2023-02-24 14:23
# * Filename      : stop.sh
# * Description   : 
# *********************************************************

docker-compose down
