#!/bin/bash
# *********************************************************
# * Author        : LEI Sen
# * Email         : sen.lei@outlook.com
# * Create time   : 2022-09-22 22:29
# * Last modified : 2022-10-27 17:42
# * Filename      : build-test.sh
# * Description   : 
# *********************************************************

v=$1

if [ "${v}" = "" ]; then
    v="latest"
    echo "No version tag provide. Using default [${v}] tag ... "
else
    echo "Version tag: ${v}"
fi


docker build -t my-umami-test:${v} -f test.Dockerfile .

