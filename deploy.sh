#!/bin/bash
# *********************************************************
# * Author        : LEI Sen
# * Email         : sen.lei@outlook.com
# * Create time   : 2022-09-22 17:08
# * Last modified : 2022-09-22 17:08
# * Filename      : deploy.sh
# * Description   : 
# *********************************************************

docker-compose -f docker-compose-bk.yml up -d
