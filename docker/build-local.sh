#!/bin/bash
# *********************************************************
# * Author        : LEI Sen
# * Email         : sen.lei@outlook.com
# * Create time   : 2022-09-22 22:29
# * Last modified : 2022-09-27 11:55
# * Filename      : build-local.sh
# * Description   : 
# *********************************************************

v=$1

if [ "${v}" = "" ]; then
    v="latest"
    echo "No version tag provide. Using default [${v}] tag ... "
else
    echo "Version tag: ${v}"
fi


docker build -t my-umami:${v} -f local.Dockerfile .

