#!/bin/bash
# *********************************************************
# * Author        : LEI Sen
# * Email         : sen.lei@outlook.com
# * Create time   : 2022-09-22 22:29
# * Last modified : 2022-09-23 11:15
# * Filename      : build_local.sh
# * Description   : 
# *********************************************************

v=$1

if [ "${v}" = "" ]; then
    v="latest"
    echo "No version tag provide. Using default [${v}] tag ... "
else
    echo "Version tag: ${v}"
fi


docker build -t umami-lite:${v} -f lite.Dockerfile .

