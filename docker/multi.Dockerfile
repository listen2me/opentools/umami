# Install dependencies only when needed
FROM --platform=$BUILDPLATFORM listen180/ls-alpine-node:latest AS deps
ARG TARGETPLATFORM
ARG BUILDPLATFORM
RUN echo "I am running on $BUILDPLATFORM, building for $TARGETPLATFORM" > /docker-build-log
FROM listen180/ls-alpine-node:latest
COPY --from=deps /docker-build-log /docker-build-log

RUN apk add --no-cache libc6-compat
WORKDIR /app
COPY package.json yarn.lock ./
#RUN yarn install --frozen-lockfile
RUN yarn install

# Rebuild the source code only when needed
FROM --platform=$BUILDPLATFORM listen180/ls-alpine-node:latest AS builder
ARG TARGETPLATFORM
ARG BUILDPLATFORM
RUN echo "I am running on $BUILDPLATFORM, building for $TARGETPLATFORM" > /docker-build-log
FROM listen180/ls-alpine-node:latest
COPY --from=builder /docker-build-log /docker-build-log

WORKDIR /app
COPY --from=deps /app/node_modules ./node_modules
COPY . .

ARG DATABASE_TYPE
ARG BASE_PATH

#ENV DATABASE_TYPE $DATABASE_TYPE
#ENV BASE_PATH $BASE_PATH
ENV DATABASE_TYPE postgresql
ENV BASE_PATH $BASE_PATH

ENV NEXT_TELEMETRY_DISABLED 1

RUN yarn build-docker
#RUN yarn build


# Production image, copy all the files and run next
FROM --platform=$BUILDPLATFORM listen180/ls-alpine-node:latest AS runner
ARG TARGETPLATFORM
ARG BUILDPLATFORM
RUN echo "I am running on $BUILDPLATFORM, building for $TARGETPLATFORM" > /docker-build-log
FROM listen180/ls-alpine-node:latest
COPY --from=runner /docker-build-log /docker-build-log


WORKDIR /app

ENV NODE_ENV production
ENV NEXT_TELEMETRY_DISABLED 1

RUN addgroup --system --gid 1001 nodejs
RUN adduser --system --uid 1001 nextjs

RUN yarn add npm-run-all dotenv prisma

# You only need to copy next.config.js if you are NOT using the default configuration
COPY --from=builder /app/next.config.js .
COPY --from=builder --chown=nextjs:nodejs /app/public ./public
COPY --from=builder /app/package.json ./package.json
COPY --from=builder /app/prisma ./prisma
COPY --from=builder /app/scripts ./scripts

# Automatically leverage output traces to reduce image size
# https://nextjs.org/docs/advanced-features/output-file-tracing
COPY --from=builder --chown=nextjs:nodejs /app/.next/standalone ./
COPY --from=builder --chown=nextjs:nodejs /app/.next/static ./.next/static

USER nextjs

EXPOSE 3000

ENV PORT 3000

CMD ["yarn", "start-docker"]
