FROM listen180/ls-alpine-node:latest AS build
RUN git clone https://github.com/umami-software/umami.git
RUN cd umami && yarn install

