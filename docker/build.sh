#!/bin/bash
# *********************************************************
# * Author        : LEI Sen
# * Email         : sen.lei@outlook.com
# * Create time   : 2022-09-22 22:29
# * Last modified : 2022-09-23 10:33
# * Filename      : build.sh
# * Description   :
# *********************************************************

v=$1

if [ "${v}" = "" ]; then
    v="latest"
    echo "No version tag provide. Using default [${v}] tag ... "
else
    echo "Version tag: ${v}"
fi


docker buildx build --platform linux/amd64,linux/arm64 -t listen180/umami:${v} -f multi.Dockerfile . --push
#docker buildx build --platform linux/amd64,linux/arm64 -t listen180/umami:${v} -f local.Dockerfile . --push

#docker buildx build --platform linux/amd64 -t listen180/umami:${v} . --push
#docker buildx build --platform linux/arm64 -t listen180/umami:${v} . --push
#docker build -t listen180/umami:${v} --build-arg DATABASE_TYPE=postgresql -f org.Dockerfile .
